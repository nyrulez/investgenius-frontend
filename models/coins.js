const client = require('./connection');

module.exports = {
	getCoins() {
		return new Promise(function (resolve, reject) {
			client.query('SELECT * FROM coins_type', function (err, data) {
				if(err) throw err;

				resolve(data.rows)
			});
		})
	},
	getCurrency() {
		return new Promise(function (resolve, reject) {
			client.query('SELECT * FROM currency', function (err, data) {
				if(err) throw err;

				resolve(data.rows)
			});
		})
	},
	addCoin(portfolioId, CoinId, quantity, rate, total_cost) {
		return new Promise(function (resolve, reject) {
			client.query('INSERT INTO coins (portfolio_id, coins_id, quantity, rate, total_cost) VALUES ($1, $2, $3, $4, $5)', [portfolioId, CoinId, quantity, rate, total_cost], function (err, data) {
				if(err) throw err;

				resolve.length ? resolve({'error' : false, 'message': 'Coin added'}) : reject(err);
			});
		})	
	},
	getCoinsTable(portfId){
		return new Promise(function (resolve, reject) {
			client.query('SELECT coins.*, coins_type.name, coins_type.logo  FROM coins JOIN coins_type ON coins.coins_id = coins_type.id WHERE coins.portfolio_id = $1 ORDER BY coins.id', [portfId], function (err, data) {
				if(err) throw err;

				resolve(data.rows)
			});
		})	
	},
	deleteCoin(coinId){
		return new Promise(function (resolve, reject) {
			client.query('DELETE FROM coins WHERE id = $1', [coinId], function (err, data) {
				if(err) throw err;

				resolve({
					'error' : false,
					'message': 'Coin deleted'
				});
			});
		})	
	},
	changeQuantityValue(TotalCost,quantityVal, coin_id){
		return new Promise(function (resolve, reject) {
			client.query('UPDATE coins SET quantity = $1,total_cost=$2 WHERE id = $3;', [quantityVal,TotalCost,coin_id], function (err, data) {
				if(err) throw err;

				resolve({
					'error' : false,
					'message': 'Coin edited'
				});
			});
		})		
	},
	changeCoinType(thisCoinId, coinId){
		console.log(coinId)
		return new Promise(function (resolve, reject) {
			client.query('UPDATE coins SET coins_id = $1 WHERE id = $2;', [thisCoinId,coinId], function (err, data) {
				if(err) throw err;
				resolve({
					'error' : false,
					'message': 'Coin edited'
				});
			});
		})
	},
	getCoinId(coin){
		return new Promise(function (resolve,reject) {
			client.query('SELECT * FROM coins_type WHERE name = $1',[coin], function (err, data) {
				if(err) throw err;

				data.rows.length?resolve(data.rows):reject('aaa')
			});
		})
	}
}