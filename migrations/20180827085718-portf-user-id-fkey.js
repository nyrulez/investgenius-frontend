'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.runSql(`
  	ALTER TABLE portfolio ADD CONSTRAINT portf_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE ON UPDATE CASCADE
  `);
};

exports.down = function(db) {
  return db.runSql(`
    ALTER TABLE portfolio DROP CONSTRAINT portf_user_id_fkey;
  `);
};

exports._meta = {
  "version": 1
};