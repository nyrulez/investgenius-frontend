var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var app = express();
Object.defineProperty(global, 'APP_PATH', {
	value: path.resolve(__dirname),
	configurable: false
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(require('cors')({
	origin: 'http://ec2-13-58-33-97.us-east-2.compute.amazonaws.com',
	credentials: true
}));

app.use(express.static(path.join(__dirname, 'front-end', 'dist')));

app.use('/api', require('./api'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json(err);
});

module.exports = app;