const { Client } = require('pg');
const { prod } = require(`${APP_PATH}/database.json`);

delete prod.driver;
const client = new Client(prod)

client.connect((err) => {
  if (err) throw err;
})

module.exports = client;