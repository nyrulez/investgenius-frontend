const client = require('./connection');

const crypto = require('crypto');

function createHash(text) {
	return crypto.createHash('sha256').update(text).digest('hex');
}

module.exports = {
	loginUser(login, password) {
		return new Promise(function (resolve, reject) {
			password = createHash(password);

			client.query('SELECT * FROM users WHERE email=$1 AND password=$2 AND status = $3', [login, password,true], function (err, data) {
				if(err) throw err;
				data.rows.length ? resolve(data.rows[0]) : reject({
					message: 'Login or password incorrect'
				});
			});
		})
	},
	addUser(data) {
		return new Promise(function (resolve, reject) {
			var { name, email, password } = data;
			password = createHash(password);

			client.query('SELECT * FROM users WHERE email = $1',[email],function(err,data){
				if (err) throw err

				if (!data.rows.length){
					client.query('INSERT INTO users (firstname, email, password, type, status) VALUES ($1, $2, $3, $4, $5)', [name, email, password, 1, false], function (err, data) {
						if(err) throw err;

						resolve({
							'email':email,
							'message': 'User added'
						});
					});
				}

				else{
					reject({
						'message': 'This Email already exist'
					})
				}	
			})			

			
		})	
	},
	confirmUser(email) {
		return new Promise (function (resolve,reject) {
			client.query ("UPDATE users SET status = $2 WHERE email = $1",[email,true],function (err,data){
				if (err) throw err

				resolve({
					message:'User confirmed'
				})
			})
		})
	},
	getUserForUpgrade(id){
		return new Promise (function (resolve,reject) {
			client.query("SELECT * FROM users WHERE id = $1",[id],function (err,data){
				if (err) throw err

					resolve(data.rows)
			})
		})
	},
	updateUser(id,name,email){
		return new Promise (function (resolve,reject) {
			client.query("UPDATE users SET firstname = $1 ,email = $2 WHERE id = $3",[name,email,id],function(err,data){
				if (err) throw err

					resolve({
						err:false,
						message:'User has updated'
					})
			})
		})
	},
	changePassword(id,password){
		password = createHash(password)

		return new Promise (function (resolve,reject) {
			client.query("UPDATE users SET password = $1 WHERE id = $2",[password,id],function(err,data){
				if (err) throw err

					resolve({
						err:false,
						message:'Password has updated'
					})
			})
		})
	}
}