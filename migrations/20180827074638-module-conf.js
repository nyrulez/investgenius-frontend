'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('module_conf', {
    id: { 
      type: 'int',
      notNull: true,
      primaryKey: true, 
      autoIncrement: true 
    },
    name: { 
      type: 'string',
      notNull: true
    },
    price: { 
      type: 'int',
      notNull: true
    },
    sort: { 
      type: 'int',
    }
  });
};

exports.down = function(db) {
  return db.dropTable('module_conf');
};

exports._meta = {
  "version": 1
};