const express        = require('express');
const router         = express.Router();
const UsersModel     = require(`${APP_PATH}/models/users`);
const CoinsModel     = require(`${APP_PATH}/models/coins`);
const PortfolioModel = require(`${APP_PATH}/models/portfolio`);
const keyPublishable = process.env.PUBLISHABLE_KEY;
const keySecret      = process.env.SECRET_KEY;
const stripe         = require("stripe")(keySecret);
const bodyParser     = require("body-parser");
const nodemailer     = require('nodemailer');


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

router.post('/login', function(req, res){
	const { login, password } = req.body;
	if(!validateEmail(login)){
		return res.status(403).json({
			errorCode: 1
		});
	}
	if(!login || !password) {
		return res.status(403).json({
			errorCode: 2
		});
	}

	const expDate = new Date();

	expDate.setDate(expDate.getDate() + (req.body.remember ? 30 : 1));

	UsersModel.loginUser(login, password).then(user => {
		res.cookie('id', user.id, { expires: expDate });
		res.cookie('name', user.firstname, { expires: expDate }).end();
	}, err => res.status(404).json(err));
});


router.post('/registration', function(req, res) {
	const keys = ['name', 'email', 'password'];
	const missingParam = keys.find(key => !req.body[key]);

	if(missingParam) {
		return res.status(403).json({
			message: `${missingParam} is required!`
		});
	}
	if(!validateEmail(req.body.email)){
		return res.status(403).json({
			message: 'Email is not valid!'
		});
	}

	UsersModel.addUser(req.body).then(data => {

	    let transporter = nodemailer.createTransport({
	        service:'gmail',
	       	secure:false,
	       	port:25,
	        auth: {
	            user: 'artur.kagaryan100@gmail.com',
	            pass: 'Kyan@rtur'
	        },
	    });

	    let mailOptions = {
	        from: '<artur.kagaryan100@gmail.com>',
	        to: data.email,
	        subject: 'Hello ✔',
	        text: 'Hello world?',
	        html: "<a href = `http://ec2-13-58-33-97.us-east-2.compute.amazonaws.com/api/confirm/"+data.email+">Confirm User</a>"
	    };

	    transporter.sendMail(mailOptions, (error, info) => {
	        if (error) {
	            return console.log(error);
	        }
	        console.log('Message sent: %s', info.messageId);
	        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
	    });

		res.status(201).json(data);

	}, err => res.status(500).json(err));
});

router.use('/confirm/:email',function (req,res){
	var mail = req.originalUrl.split('/').pop()

	UsersModel.confirmUser(mail).then((data)=>{
		res.redirect('/')
	})

});

router.post('/getCoins', function(req, res){
	CoinsModel.getCoins().then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});

router.post('/getCurrency', function(req, res){
	CoinsModel.getCurrency().then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});

router.post('/getPortfolios', function(req, res){
	const userID = req.body.userId;
	PortfolioModel.getPortfolios(userID).then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});
router.post('/addPortfolio', function(req, res){
	const { userId, name, currency_id } = req.body;
	PortfolioModel.addPortfolio(userId, name, currency_id).then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});

router.post('/deletePortfolio', function(req, res){
	const portfolioId = req.body.portfolioId;
	PortfolioModel.deletePortfolio(portfolioId).then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});

router.post('/renamePortfolio', function(req, res){
	const {portfolioId, portfolioName} = req.body;
	PortfolioModel.renamePortfolio(portfolioId, portfolioName).then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});

router.post('/addCoin', function(req, res){
	const {portfolioId, CoinId, quantity, rate, total_cost} = req.body;
	CoinsModel.addCoin(portfolioId, CoinId, quantity, rate, total_cost).then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});

router.post('/getCoinsTable', function(req, res){
	const portfId = req.body.portfId;
	CoinsModel.getCoinsTable(portfId).then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});

router.post('/getCurrName', function(req, res){
	const portfId = req.body.portfId;
	PortfolioModel.getCurrName(portfId).then(data => {
		res.status(201).json(data[0]);
	}, err => res.status(404).json(err));
});

router.post('/getModuleConfigurations', function(req, res){
	const portfId = req.body.portfId;
	PortfolioModel.getModuleConfigurations(portfId).then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});

router.post('/deleteCoin', function(req, res){
	const coinId = req.body.coinId;
	CoinsModel.deleteCoin(coinId).then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});

router.post('/addModuleConf', function(req, res){
	const {modulConfId, portfId} = req.body;
	PortfolioModel.addModuleConf(modulConfId, portfId).then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});

router.post('/removeModuleConf', function(req, res){
	const {modulConfId, portfId} = req.body;
	PortfolioModel.removeModuleConf(modulConfId, portfId).then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});

router.post('/changeQuantityValue', function(req, res){
	const {rateVal,quantityVal, coin_id} = req.body;
	var TotalCost = rateVal*quantityVal
	CoinsModel.changeQuantityValue(TotalCost,quantityVal, coin_id).then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});

router.post('/getSumPortfValue', function(req, res){
	portfId = req.body.portfId;
	PortfolioModel.getSumPortfValue(portfId).then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});

router.post('/changeCoinType', function(req, res){
	const {thisCoinId, coinId} = req.body;
	CoinsModel.changeCoinType(thisCoinId, coinId).then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});

router.post('/payment',function(req,res){
  	var stripe = require("stripe")("sk_test_TLcmb9cObRQjQpclSCFFnRyF");

  	const token = req.body.token.id;

  	const charge = stripe.charges.create({
  	  amount: 2000,
  	  currency: 'USD',
  	  description: 'Example charge',
  	  source: token,
  	}).then((data)=>{
  		res.status(200).json(data);
  	})
})

router.post('/getUpgradeValue', function(req, res){
	portfId = req.body.portfolioId;
	PortfolioModel.getUpgradeValue(portfId).then(data => {
		res.status(201).json(data);
	}, err => res.status(404).json(err));
});

router.post('/UpgradeUser',function(req,res){
	var {portfolioId,userId} = req.body
	PortfolioModel.UpgradePortfoloi(portfolioId,userId).then(data=>{
		res.status(200).json(data);
	})
});

router.post('/IsUpgrated',function(req,res){
	var {portfolioId,userId} = req.body
	PortfolioModel.IsUpgrated(portfolioId,userId).then(data=>{
		res.status(200).json(data)
	},(data)=>{
		res.status(200).json(data)
	})
})

router.post('/getCoinId',function(req,res){
	var {coinName} = req.body

	CoinsModel.getCoinId(coinName).then(data=>{
		res.status(200).json(data[0].id)
	},err=>{
		res.status(200)
	})
})

router.post('/sortList',function(req,res){
	var {data} = req.body

	PortfolioModel.sortList(data).then((data)=>{
		res.status(200).json(data)
	})
})

router.post('/getUserForUpgrade',function(req,res){
	UsersModel.getUserForUpgrade(req.body.id).then((result)=>{
		res.status(200).json(result)
	})
})

router.post('/updateUser',function(req,res){
	var {id,name,email} = req.body

	if (id && name && email) {
		UsersModel.updateUser(id,name,email).then((result)=>{
			res.status(200).json(result)
		})
	}
	else{
		res.status(200).json({message:'All fields are required',err:true})
	}
})

router.post('/changePassword',function(req,res){
	var {id,password,confirm} = req.body

	if (id && password && confirm) {
		if(password == confirm){
			UsersModel.changePassword(id,password).then((result)=>{
				res.status(200).json(result)
			})
		}
	}
	else{
		res.status(200).json({message:'All fields are required',err:true})
	}
})

module.exports = router;
