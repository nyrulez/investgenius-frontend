'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('coins', {
    id: { 
      type: 'int',
      notNull: true,
      primaryKey: true, 
      autoIncrement: true 
    },
    portfolio_id: { 
      type: 'int',
      notNull: true
    },
    coins_id: { 
      type: 'int',
      notNull: true
    },
    quantity: { 
      type: 'float',
      notNull: true
    },
    rate: { 
      type: 'float',
      notNull: true
    },
    total_cost: { 
      type: 'float',
      notNull: true
    }
  });
};

exports.down = function(db) {
  return db.dropTable('coins');
};

exports._meta = {
  "version": 1
};
