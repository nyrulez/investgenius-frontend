const client = require('./connection');

module.exports = {
	getPortfolios(userID) {
		return new Promise(function (resolve, reject) {
			client.query('SELECT portfolio.*, currency.name as cur_name FROM portfolio JOIN currency ON portfolio.currency_id = currency.id WHERE user_id = $1 ORDER BY portfolio.id', [userID], function (err, data) {
				if(err) throw err;

				resolve(data.rows)
			});
		})
	},
	addPortfolio(userId, name, currency_id) {
		return new Promise(function (resolve, reject) {
			client.query('INSERT INTO portfolio (name, currency_id, user_id) VALUES ($1, $2, $3) RETURNING id', [name, currency_id, userId], function (err, data) {
				if(err) throw err;

				resolve({
					'error' : false,
					'response': data.rows
				});
			});
		})	
	},
	deletePortfolio(portfolioId){
		return new Promise(function (resolve, reject) {
			client.query('DELETE FROM portfolio WHERE id = $1', [portfolioId], function (err, data) {
				if(err) throw err;

				resolve({
					'error' : false,
					'message': 'Portfolio deleted'
				});
			});
		})	
	},
	renamePortfolio(portfolioId, portfolioName) {
		return new Promise(function (resolve, reject) {
			client.query('UPDATE portfolio SET name = $1 WHERE id = $2', [portfolioName, portfolioId ], function (err, data) {
				if(err) throw err;

				resolve({
					'error' : false,
					'message': 'Portfolio renamed'
				});
			});
		})		
	},
	getCurrName(curr_id){
		return new Promise(function (resolve, reject) {
			client.query('SELECT currency.* FROM portfolio JOIN currency ON portfolio.currency_id = currency.id WHERE portfolio.id = $1', [curr_id], function (err, data) {
				if(err) throw err;

				resolve(data.rows)
			});
		})	
	},
	getModuleConfigurations(portfId){
		return new Promise(function (resolve, reject) {
			client.query('SELECT module_conf.*, portfolio_module.id as checked FROM module_conf left join portfolio_module on portfolio_module.id_module_conf = module_conf.id and portfolio_module.id_portfolio = $1', [portfId], function (err, data) {
				if(err) throw err;

				resolve(data.rows)
			});
		})
	},
	addModuleConf(modulConfId, portfId){
		return new Promise(function (resolve, reject) {
			client.query('INSERT INTO portfolio_module (id_portfolio, id_module_conf) VALUES ($1, $2)', [portfId, modulConfId] , function (err, data) {
				if(err) throw err;

				resolve({
					'error' : false,
					'message': 'Module Configuration added'
				});
			});
		})
	},
	removeModuleConf(modulConfId, portfId) {
		return new Promise(function (resolve, reject) {
			client.query('DELETE FROM portfolio_module WHERE id_portfolio = $1 and id_module_conf = $2', [portfId, modulConfId] , function (err, data) {
				if(err) throw err;

				resolve({
					'error' : false,
					'message': 'Module Configuration deleted'
				});
			});
		})	
	},
	getSumPortfValue(portfId){
		return new Promise(function (resolve, reject) {
			client.query('SELECT SUM(total_cost) as portfValue FROM coins WHERE portfolio_id = $1',[portfId], function (err, data) {
				if(err) throw err;

				resolve({
					'error' : false,
					'response': data.rows
				});
			});
		})
	},
	getUpgradeValue(portfId){
		return new Promise(function (resolve, reject) {
			client.query('SELECT SUM(module_conf.price) as price FROM module_conf LEFT JOIN portfolio_module on portfolio_module.id_module_conf = module_conf.id WHERE portfolio_module.id_portfolio = $1',[portfId], function (err, data) {
				if(err) throw err;

				resolve({
					'error' : false,
					'response': data.rows
				});
			});
		})
	},
	UpgradePortfoloi(portfolioId,userId){
		return new Promise(function(resolve,reject){
			client.query('INSERT INTO upgraded (portfolio_id,user_id) VALUES ($1, $2)',[portfolioId,userId],function(err,data){
				if (err) throw err

				resolve({
					'error':false,
					'response':data
				})
			})
		})
	},
	IsUpgrated(portfolioId,userId){
		return new Promise (function(resolve,reject){
			client.query('SELECT * FROM upgraded WHERE portfolio_id = $1 and user_id = $2',[portfolioId,userId],function(err,data){
				if (err) throw err

					data.rows.length?resolve(true):reject(false)
			})

		})
	},
	sortList(data){
		return new Promise (function (resolve,reject){
			
			
			for (var i = 0; i < data.length; i++) {
				client.query('UPDATE module_conf SET name = $1 , price = $2 , sort = $3 WHERE id = $4' ,[data[i].name,data[i].price,data[i].sort,data[i].id],function (err,data){
					if (err) throw err

						resolve({
							'message':'updated'
						})
				})
			}
		})
	}
}