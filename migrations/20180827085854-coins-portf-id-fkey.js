'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.runSql(`
  	ALTER TABLE coins ADD CONSTRAINT coins_portf_id_fkey FOREIGN KEY (portfolio_id) REFERENCES public.portfolio(id) ON DELETE CASCADE ON UPDATE CASCADE
  `);
};

exports.down = function(db) {
  return db.runSql(`
    ALTER TABLE coins DROP CONSTRAINT coins_portf_id_fkey;
  `);
};

exports._meta = {
  "version": 1
};